import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Model.CalculoSimulacao01;


public class ep2test {
	
	public double AmplitudeTensao;
	public double AmplitudeCorrente;
	public double AnguloTensao;
	public double AnguloCorrente;
	CalculoSimulacao01 simulacao01;

	@Before
	public void Beforetest() {
		
		AmplitudeTensao = 220.0;
		AmplitudeCorrente = 39.0;
		
		AnguloTensao = 35.0;
		AnguloCorrente = 0.0;
		
		simulacao01 = new CalculoSimulacao01();
	
	}
	
	@Test
	public void testaConstrutor(){
		
		simulacao01 = new CalculoSimulacao01();
		
		assertEquals(AmplitudeTensao, simulacao01.getAmplitudeTensao(),10e-5);
		assertEquals(AmplitudeCorrente, simulacao01.getAmplitudeCorrente(), 10e-5);
		assertEquals(AnguloTensao, simulacao01.getAnguloCorrente(), 10e-5);
		assertEquals(AnguloCorrente, simulacao01.getAnguloCorrente(), 10e-5);
		
	}
	
	@Test
	public void testaPotenciaAtiva(){
		
		simulacao01 = new CalculoSimulacao01();
		
		double potenciaAtiva = 7028;
		
		assertEquals(potenciaAtiva, simulacao01.PotenciaAtiva(),10e-5);
		
	}
	
	@Test
	public void testaPotenciaReativa(){
		
		simulacao01 = new CalculoSimulacao01();
		
		double potenciaReativa = 4921;
		
		assertEquals(potenciaReativa, simulacao01.PotenciaReativa(), 10e-5);
	}
	
	@Test
	public void testaPotenciaAparente(){
		simulacao01 = new CalculoSimulacao01();
		
		double potenciaAparente = 8580;
		
		assertEquals(potenciaAparente , simulacao01.PotenciaAparente(), 10e-5);
		
	}

	@After
	void afterTests(){
		simulacao01 = null;
	}

}
