# Instruções EP2
```
Nome: João Pedro Gomes Cabral Ferreira
Matricula: 14/0145842
```

**FAÇA O DOWNLOAD DO PROGRAMA COMPACTADO**

![tela6](/uploads/1a2c00cb1b9aa3c3e6098dcd55f724a8/tela6.png)


**OU SIMPLESMENTE CLONE O REPOSITÓRIO PARA SUA MAQUINA**

```
git clone https://gitlab.com/joaopedrogomes/ep2.git

```

**ENTRE NA IDE DO ECLIPSE**

![tela1](/uploads/0fb3517acb0d45835b319fbf873a6f7f/tela1.png)


**VÀ EM NEW PROJECT**

![tela2](/uploads/58542bb7c25aea438aba021ab1fabec3/tela2.png)


**CRIE UM PROJETO COM QUALQUER NOME GENÉRICO**

![tela3](/uploads/c4b2f1120199ecd21b49ba5c4fc58a33/tela3.png)


**COM O BOTÃO DIREITO NO NOVO PROJETO E CLIQUE EM IMPORT**

![tela4](/uploads/4bec03ec86176e1d4b1f3d1b4b8bb848/tela4.png)


**SELECIONE File System e selecione o diretório que foi clonado ou baixado (tem que estár descompactado)**

![tela5](/uploads/44d53bd38cae4d06f408fa7b13096fab/tela5.png)

**MARQUE A OPÇÃO E CLIQUE EM FINISH**

![tela02](/uploads/b7d7989c853e2b373ba97a0493a3fb87/tela02.png)


**VÁ NO PACKAGE VIEW MENU INICIAL E RODE O PROGRAMA CLICANDO EM F11**

![tela01](/uploads/1780fa88eb109330aa3ec0001abb836f/tela01.png)


# ENJOY

**OBS: NÃO FOI CRIADO NENHUM EXECUTÁVEL POIS TIVE ALGUNS PROBLEMAS**








* Manual do usuário;
* Quais pontos dos critérios de aceitação foram implementados;
