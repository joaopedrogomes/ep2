package Model;
import java.lang.Math;
import java.util.ArrayList;

public class CalculoSimulacao01 {

	
	private double AmplitudeTensao, AmplitudeCorrente, AnguloTensao, AnguloCorrente;
	
	
// ---------------------------------------------------------------------------------------------		
	
	public double getAmplitudeTensao (){
		return AmplitudeTensao;
	}

// ---------------------------------------------------------------------------------------------	
	public double getAnguloTensao (){
		return AnguloTensao;
	}

// ---------------------------------------------------------------------------------------------	

	public double getAmplitudeCorrente(){
			return AmplitudeCorrente;
	}
	

// --------------------------------------------------------------------------------------------	
	public double getAnguloCorrente (){
		return AnguloCorrente;
	}

// ---------------------------------------------------------------------------------------------		
	public void setAnguloTensao(double AnguloTensao){
		this.AnguloTensao = AnguloTensao;
	}

// ---------------------------------------------------------------------------------------------		
	public void setAmplitudeTensao(double AmplitudeTensao){
		this.AmplitudeTensao = AmplitudeTensao;
	}

// ---------------------------------------------------------------------------------------------	
	public void setAnguloCorrente(double AnguloCorrente){
			this.AnguloCorrente = AnguloCorrente;
	}

// ---------------------------------------------------------------------------------------------		

	public void setAmplitudeCorrente(double AmplitudeCorrente){
		this.AmplitudeCorrente = AmplitudeCorrente;
	}
	
// ---------------------------------------------------------------------------------------------		
	public double PotenciaAtiva () {
		
		double PotAtiva = getAmplitudeTensao() * getAmplitudeCorrente() * Math.cos(Math.toRadians(getAnguloTensao() - getAnguloCorrente()));
		
		PotAtiva = Math.round(PotAtiva * 1.0)/1.0;
		
		return PotAtiva;
		
	}
// ---------------------------------------------------------------------------------------------		
	public double PotenciaReativa () {
		
		double PotReativa = getAmplitudeTensao() * getAmplitudeCorrente() * Math.sin(Math.toRadians(getAnguloTensao()- getAnguloCorrente()));
		
		PotReativa = Math.round(PotReativa * 1.0)/1.0;
		
		return PotReativa;
	}

// ---------------------------------------------------------------------------------------------	
	public double PotenciaAparente () {
		
		double PotAparente = getAmplitudeTensao() * getAmplitudeCorrente();
		
		PotAparente = Math.round(PotAparente *1.0)/1.0;
		
		return PotAparente;
	}
// ---------------------------------------------------------------------------------------------		
	public double FatorPotencia () {
		
		double FatPot = Math.cos(Math.toRadians(getAnguloTensao()- getAnguloCorrente()));
		FatPot = Math.round(FatPot*1000.0)/1000.0;
		return FatPot;
	}

//--------------------------------------------------------------------------------------------------------------------
	
	public double AnguloFi(){
		
		double anguloFi = getAnguloTensao() - getAnguloCorrente();
		
		return anguloFi;
	}
// ====================================================== FUNÇÕES PARA O GRÁFICO ========================================
	
	public ArrayList<Double> InserePontosPotenciaInstantanea(){
		
		setAmplitudeTensao(AmplitudeTensao);
		setAnguloTensao(AnguloTensao);
		setAmplitudeCorrente(AmplitudeCorrente);
		setAnguloCorrente(AnguloCorrente);
		
		ArrayList <Double> grafico0 = new ArrayList<>();
		
		for(double i = 0; i<=50; i = (double)(i+0.05)){
			
			grafico0.add(getAmplitudeTensao()*Math.cos((2*3.14*60*i) + getAnguloTensao())*(getAmplitudeCorrente()*Math.cos(2*3.14*60*i) + getAnguloCorrente()));			
		}
		
		return (grafico0);
	}
	
	
	//ARRAY DE DOUBLE - FORMA DE ONDA DA TENSÃO
	public ArrayList<Double> InserePontosOndaTensao() {
		
		setAmplitudeTensao(AmplitudeTensao);
		setAnguloTensao(AnguloTensao);
		
		ArrayList<Double> grafico1 = new ArrayList<>();
		
		for(double i = 0; i<=50; i = (double)(i+0.05)){
			
			grafico1.add(getAmplitudeTensao()*Math.cos((2*3.14*60*i) + getAnguloTensao()));
		}
		
		return(grafico1);
	}
	
	public ArrayList<Double> InserePontosOndaCorrente() {
		
		setAmplitudeCorrente(AmplitudeCorrente);
		setAnguloCorrente(AnguloCorrente);
		
		ArrayList<Double> grafico2 = new ArrayList<>();
		
		for(double i = 0; i<=50; i = (double)(i+0.05)){
			
			grafico2.add(getAmplitudeCorrente()*Math.cos(2*3.14*60*i) + getAnguloCorrente());
		}
		
		return(grafico2);
		
	}
	
	
	
	
	
	
}
