package Model;
import java.lang.Math;
import java.util.ArrayList;

public class CalculoSimulacao02 {

	
	private double amplitudeComponenteFundamental, anguloComponenteFundamental;
	private double amplitudeHarmonico1, amplitudeHarmonico2,amplitudeHarmonico3,amplitudeHarmonico4,amplitudeHarmonico5,amplitudeHarmonico6;
	private double ordemHarmonico1,ordemHarmonico2,ordemHarmonico3,ordemHarmonico4,ordemHarmonico5,ordemHarmonico6;
	private double anguloHarmonico1,anguloHarmonico2,anguloHarmonico3,anguloHarmonico4,anguloHarmonico5,anguloHarmonico6;
// -----------------------GET----Amplitude COMPONENTE fundamental------------------------------------------------------------------		
	
	public double getAmplitudeComponenteFundamental() {
        
		return amplitudeComponenteFundamental;
    }
// -----------------------SET---AMPLITUDE COMPONENTE FUNDAMENTAL-------------------------------------------------------------------	
	
	public void setAmplitudeComponenteFundamental (double amplitudeComponenteFundamental){
		this.amplitudeComponenteFundamental = amplitudeComponenteFundamental;
	}
// -----------------------GET ANGULO COMPONENTE FUNDAMENTAL----------------------------------------------------------------------	

	public double getAnguloComponenteFundamental(){
			return anguloComponenteFundamental;
	}
	
// -----------------------SET ANGULO COMPONENTE FUNDAMENTAL---------------------------------------------------------------------	
	public void setAnguloComponenteFundamental (double anguloComponenteFundamental){
		this.anguloComponenteFundamental = anguloComponenteFundamental;
	}
	
// -----------------------GET AMPLITUDE HARMONICO 1---------------------------------------------------------------------	
	public double getAmplitudeHarmonico1(){
		return amplitudeHarmonico1;
	}

// -----------------------SET AMPLITUDE HARMONICO 1---------------------------------------------------------------------	
	public void setAmplitudeHarmonico1(double amplitudeHarmonico1){
		this.amplitudeHarmonico1 = amplitudeHarmonico1;
	}
	
// ----------------------GET AMPLITUDE HARMONICO 2---------------------------------------------------------------------
	public double getAmplitudeHarmonico2(){
		return amplitudeHarmonico2;
	}
	
// -----------------------SET AMPLITUDE HARMONICO 2---------------------------------------------------------------------
	public void setAmplitudeHarmonico2(double amplitudeHarmonico2){
		this.amplitudeHarmonico2 = amplitudeHarmonico2;
	}
	
// -----------------------GET AMPLITUDE HARMONICO 3---------------------------------------------------------------------
	public double getAmplitudeHarmonico3(){
		return amplitudeHarmonico3;
	}
	
// -----------------------SET AMPLITUDE HARMONICO 3---------------------------------------------------------------------
	public void setAmplitudeHarmonico3(double amplitudeHarmonico3){
		this.amplitudeHarmonico3 = amplitudeHarmonico3;
	}

// -----------------------GET AMPLITUDE HARMONICO 4---------------------------------------------------------------------
	public double getAmplitudeHarmonico4(){
		return amplitudeHarmonico4;
	}
// -----------------------SET AMPLITUDE HARMONICO 4---------------------------------------------------------------------
	public void setAmplitudeHarmonico4(double amplitudeHarmonico4){
		this.amplitudeHarmonico4 = amplitudeHarmonico4;
	}
// -----------------------GET AMPLITUDE HARMONICO 5---------------------------------------------------------------------	
	public double getAmplitudeHarmonico5(){
		return amplitudeHarmonico5;
	}
	
// -----------------------SET AMPLITUDE HARMONICO 5---------------------------------------------------------------------	
	public void setAmplitudeHarmonico5(double amplitudeHarmonico5){
		this.amplitudeHarmonico5 = amplitudeHarmonico5;
	}
		
// -----------------------GET AMPLITUDE HARMONICO 6---------------------------------------------------------------------
	
	public double getAmplitudeHarmonico6(){
		return amplitudeHarmonico6;
	}
// -----------------------SET AMPLITUDE HARMONICO 6---------------------------------------------------------------------
	public void setAmplitudeHarmonico6(double amplitudeHarmonico6){
		this.amplitudeHarmonico6 = amplitudeHarmonico6;
	}

	// -----------------------GET ORDEM HARMONICO 1---------------------------------------------------------------------

	public double getOrdemHarmonico1(){
		return ordemHarmonico1;
	}
	
	// -----------------------SET ORDEM HARMONICO 1---------------------------------------------------------------------
	
	public void setOrdemHarmonico1(double ordemHarmonico1){
		this.ordemHarmonico1 = ordemHarmonico1;
	}
	
	// -----------------------GET ORDEM HARMONICO 2---------------------------------------------------------------------
	
	public double getOrdemHarmonico2(){
		return ordemHarmonico2;
	}
	
	// -----------------------SET ORDEM HARMONICO 2---------------------------------------------------------------------
	
	public void setOrdemHarmonico2(double ordemHarmonico2){
		this.ordemHarmonico2= ordemHarmonico2;
	}
	
	// -----------------------GET ORDEM HARMONICO 3---------------------------------------------------------------------
	
	public double getOrdemHarmonico3(){
		return ordemHarmonico3;
	}
	
	// -----------------------SET ORDEM HARMONICO 3---------------------------------------------------------------------
	
	public void setOrdemHarmonico3(double ordemHarmonico3){
		this.ordemHarmonico3 = ordemHarmonico3;
	}
	// -----------------------GET ORDEM HARMONICO 4---------------------------------------------------------------------
	
	public double getOrdemHarmonico4(){
		return ordemHarmonico4;
	}
	// -----------------------SET ORDEM HARMONICO 4---------------------------------------------------------------------
	
	public void setOrdemHarmonico4(double ordemHarmonico4){
		this.ordemHarmonico4 = ordemHarmonico4;
	}
	// -----------------------GET ORDEM HARMONICO 5---------------------------------------------------------------------
	public double getOrdemHarmonico5(){
		return ordemHarmonico5;
	}
	
	
	// -----------------------SET ORDEM HARMONICO 5---------------------------------------------------------------------
	
	public void setOrdemHarmonico5(double ordemHarmonico5){
		this.ordemHarmonico5 = ordemHarmonico5;
	}
	// -----------------------GET ORDEM HARMONICO 6---------------------------------------------------------------------
	
	public double getOrdemHarmonico6(){
		return ordemHarmonico6;
	}
	
	// -----------------------SET ORDEM HARMONICO 6---------------------------------------------------------------------
	
	public void setOrdemHarmonico6(double ordemHarmonico6){
		this.ordemHarmonico6 = ordemHarmonico6;
	}

	// -----------------------GET ANGULO HARMONICO 1---------------------------------------------------------------------
	
	public double getAnguloHarmonico1(){
		return anguloHarmonico1;
	}
	
	// -----------------------SET ANGULO HARMONICO 1---------------------------------------------------------------------
	
	public void setAnguloHarmonico1(double anguloHarmonico1){
		this.anguloHarmonico1 = anguloHarmonico1;
	}
	// -----------------------GET ANGULO HARMONICO 2---------------------------------------------------------------------
	
	public double getAnguloHarmonico2(){
		return anguloHarmonico2;
	}
	// -----------------------SET ANGULO HARMONICO 2---------------------------------------------------------------------
	
	public void setAnguloHarmonico2(double anguloHarmonico2){
		this.anguloHarmonico2 = anguloHarmonico2;
	}
	
	// -----------------------GET ANGULO HARMONICO 3---------------------------------------------------------------------
	
	public double getAnguloHarmonico3(){
		return anguloHarmonico3;
	}
	
	// -----------------------SET ANGULO HARMONICO 3---------------------------------------------------------------------
	
	public void setAnguloHarmonico3(double anguloHarmonico3){
		this.anguloHarmonico3 = anguloHarmonico3;
	}
	
	// -----------------------GET ANGULO HARMONICO 4---------------------------------------------------------------------
	
	public double getAnguloHarmonico4(){
	
		return anguloHarmonico4;
	}
	// -----------------------SET ANGULO HARMONICO 4---------------------------------------------------------------------
	
	public void setAnguloHarmonico4(double anguloHarmonico4){
		
		this.anguloHarmonico4 = anguloHarmonico4;
	}
	
	// -----------------------GET ANGULO HARMONICO 5---------------------------------------------------------------------
	
	public double getAnguloHarmonico5(){
		return anguloHarmonico5;
	}
	
	// -----------------------SET ANGULO HARMONICO 5---------------------------------------------------------------------
	
	public void setAnguloHarmonico5(double anguloHarmonico5){
		this.anguloHarmonico6 = anguloHarmonico5;
	}
	
	// -----------------------GET ANGULO HARMONICO 6---------------------------------------------------------------------
	
	public double getAnguloHarmonico6(){
	
		return anguloHarmonico6;
	}
	
	// -----------------------SET ANGULO HARMONICO 6---------------------------------------------------------------------
	
	public void setAnguloHarmonico6(double anguloHarmonico6){
		
		this.anguloHarmonico6 =anguloHarmonico6;
	}
	
	
	
		
//-------------------------------------COMPONENTE FUNDAMENTAL ---------------------------------------------------------
	public ArrayList<Double> InserePontosComponenteFundamental()
	{
		ArrayList<Double> formaDeOndaCompFundamental = new ArrayList<>();
		
		for(double j = 0; j <=50; j = (float) (j+0.05)){
			
			formaDeOndaCompFundamental.add(getAmplitudeComponenteFundamental()*Math.cos((2*3.14*60*j) + getAnguloComponenteFundamental()));
		}
			return (formaDeOndaCompFundamental);
	}
//-=--------------------------------------------------------------------------------------------------------------------------------------	
	
	public ArrayList<Double> ondaHarmonico1(){
        ArrayList<Double> graficoHarmonico1 = new ArrayList<>();

        for (double i = 0; i <= 50; i = (float) (i + 0.05)) {
            graficoHarmonico1.add(getAmplitudeHarmonico1()*Math.cos((getOrdemHarmonico1()*2*3.14*60*i) + getAnguloHarmonico1()));
        }
        return (graficoHarmonico1);
	}

	//-=--------------------------------------------------------------------------------------------------------------------------------------	
	
		public ArrayList<Double> ondaHarmonico2(){
	        ArrayList<Double> graficoHarmonico2 = new ArrayList<>();

	        for (double i = 0; i <=50; i = (float) (i + 0.05)) {
	           graficoHarmonico2.add(getAmplitudeHarmonico2()*Math.cos((getOrdemHarmonico2()*2*3.14*60*i) + getAnguloHarmonico2()));
	        }
	        return (graficoHarmonico2);
		}
	
//-=--------------------------------------------------------------------------------------------------------------------------------------	
		
			public ArrayList<Double> ondaHarmonico3(){
		        ArrayList<Double> graficoHarmonico3 = new ArrayList<>();

		        for (double i = 0; i <= 50; i = (float) (i + 0.05)) {
		            graficoHarmonico3.add(getAmplitudeHarmonico3()*Math.cos((getOrdemHarmonico3()*2*3.14*60*i) + getAnguloHarmonico3()));
		        }
		        return (graficoHarmonico3);
			}
	
	
//-=--------------------------------------------------------------------------------------------------------------------------------------	

public ArrayList<Double> ondaHarmonico4(){
    ArrayList<Double> graficoHarmonico4 = new ArrayList<>();

    for (double i = 0; i <=50; i = (float) (i + 0.05)) {
        graficoHarmonico4.add(getAmplitudeHarmonico4()*Math.cos((getOrdemHarmonico4()*2*3.14*60*i) + getAnguloHarmonico4()));
    }
    return (graficoHarmonico4);
}

//-=--------------------------------------------------------------------------------------------------------------------------------------	

public ArrayList<Double> ondaHarmonico5(){
    ArrayList<Double> graficoHarmonico5 = new ArrayList<>();

    for (double i = 0; i <=50; i = (float) (i + 0.05)) {
        graficoHarmonico5.add(getAmplitudeHarmonico5()*Math.cos((getOrdemHarmonico5()*2*3.14*60*i) + getAnguloHarmonico5()));
    }
    return (graficoHarmonico5);
}



//-=--------------------------------------------------------------------------------------------------------------------------------------	

public ArrayList<Double> ondaHarmonico6(){
    ArrayList<Double> graficoHarmonico6 = new ArrayList<>();

    for (double i = 0; i <=50; i = (float) (i + 0.05)) {
        graficoHarmonico6.add(getAmplitudeHarmonico6()*Math.cos((getOrdemHarmonico6()*2*3.14*60*i) + getAnguloHarmonico6()));
    }
    return (graficoHarmonico6);
}



//-----------------------------------------------------------------------------------------------------------------------------------------------------

public ArrayList<Double> ondaResultante(){
	
    ArrayList<Double> graficoResultante = new ArrayList<>();

    for (double i = 0; i <=50; i = (float) (i + 0.05)) {
        graficoResultante.add(getAmplitudeComponenteFundamental()*Math.cos((2*3.14*60*i) + getAnguloComponenteFundamental())+
                getAmplitudeHarmonico1()*Math.cos((getOrdemHarmonico1()*2*3.14*60*i) + getAnguloHarmonico1()) +
                getAmplitudeHarmonico2()*Math.cos((getOrdemHarmonico2()*2*3.14*60*i) + getAnguloHarmonico2()) +
                getAmplitudeHarmonico3()*Math.cos((getOrdemHarmonico3()*2*3.14*60*i) + getAnguloHarmonico3()) +
                getAmplitudeHarmonico4()*Math.cos((getOrdemHarmonico4()*2*3.14*60*i) + getAnguloHarmonico4()) +
                getAmplitudeHarmonico5()*Math.cos((getOrdemHarmonico5()*2*3.14*60*i) + getAnguloHarmonico5()) +
                getAmplitudeHarmonico6()*Math.cos((getOrdemHarmonico6()*2*3.14*60*i) + getAnguloHarmonico6())          
        );
    }
    return (graficoResultante);
}

}




	
	
	
	
