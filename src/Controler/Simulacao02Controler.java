package Controler;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.JPanel;
import javax.swing.JTextField;


import Model.CalculoSimulacao02;
//import View.Simulacao01;
import View.Grafico;

public class Simulacao02Controler implements ActionListener 
{
	CalculoSimulacao02 simulacao02 = new CalculoSimulacao02();

	private JFrame mainFrame1;
	private JTextField textAmplitudeFundamental;
	private JTextField textAnguloDeFase;
	private JTextField textNumeroOrdensHarmonicas;
	private JTextField textAmplitude1;
	private JTextField textOrdem1;
	private JTextField textAngulo1;
	private JTextField textAmplitude2;
	private JTextField textOrdem2;
	private JTextField textAngulo2;
	private JTextField textAmplitude3;
	private JTextField textOrdem3;
	private JTextField textAngulo3;
	private JTextField textAmplitude4;
	private JTextField textOrdem4;
	private JTextField textAngulo4;
	private JTextField textAmplitude5;
	private JTextField textOrdem5;
	private JTextField textAngulo5;
	private JTextField textAmplitude6;
	private JTextField textOrdem6;
	private JTextField textAngulo6;
	private Grafico graficoResultante;
	private Grafico graficoHarmonico1;
	private Grafico graficoHarmonico2;
	private Grafico graficoHarmonico3;
	private Grafico graficoHarmonico4;
	private Grafico graficoHarmonico5;
	private Grafico graficoHarmonico6;
	private JComboBox comboBoxNumeroHarmonicas;
	

	
	
	public Simulacao02Controler(JComboBox comboBoxNumeroHarmonicas,JFrame mainFrame1, JTextField textAmplitudeFundamental,JTextField textAnguloDeFase,
			Grafico graficoHarmonico1, Grafico graficoHarmonico2, Grafico graficoHarmonico3,
			Grafico graficoHarmonico4, Grafico graficoHarmonico5, Grafico graficoHarmonico6,
			Grafico graficoComponenteFundamental,JTextField textAmplitude1,
			Grafico graficoResultante, JTextField textOrdem1,JTextField textAngulo1,
			JTextField textAmplitude2,	JTextField textOrdem2,JTextField textAngulo2,
			JTextField textAmplitude3, JTextField textOrdem3,JTextField textAngulo3,
			JTextField textAmplitude4,JTextField textOrdem4, JTextField textAngulo4,
			JTextField textAmplitude5, JTextField textOrdem5, JTextField textAngulo5, 
			JTextField textAmplitude6, JTextField textOrdem6, JTextField textAngulo6){
		
			this.comboBoxNumeroHarmonicas = comboBoxNumeroHarmonicas;
			this.mainFrame1 = mainFrame1;
			this.textAmplitudeFundamental = textAmplitudeFundamental;
			this.textAnguloDeFase = textAnguloDeFase;
			this.graficoHarmonico1 = graficoHarmonico1;
			this.graficoHarmonico2 = graficoHarmonico2;
			this.graficoHarmonico3 = graficoHarmonico3;
			this.graficoHarmonico4 = graficoHarmonico4;
			this.graficoHarmonico5 = graficoHarmonico5;
			this.graficoHarmonico6 =  graficoHarmonico6;
			this.graficoResultante = graficoResultante;
			this.textAmplitude1 = textAmplitude1;
			this.textOrdem1 = textOrdem1;
			this.textAngulo1 = textAngulo1;
			this.textAmplitude2 = textAmplitude2;
			this.textOrdem2 = textOrdem2;
			this.textAngulo2 = textAngulo2;
			this.textAmplitude3= textAmplitude3;
			this.textOrdem3 = textOrdem3;
			this.textAngulo3 = textAngulo3;
			this.textAmplitude4 = textAmplitude4;
			this.textOrdem4 = textOrdem4;
			this.textAngulo4 = textAngulo4;
			this.textAmplitude5 = textAmplitude5;
			this.textOrdem5 = textOrdem5;
			this.textAngulo5 = textAngulo5;
			this.textAmplitude6 = textAmplitude6;
			this.textOrdem6 = textOrdem6;
			this.textAngulo6 = textAngulo6;
	}

	public void actionPerformed(ActionEvent e){
		
		double ampFundamental, angFundamental;
		int numeroHarmonicas;
		double ang1,ang2,ang3,ang4,ang5,ang6;
		double amp1,amp2,amp3,amp4,amp5,amp6;
		double ord1,ord2,ord3,ord4,ord5,ord6;
	
				ampFundamental = Double.parseDouble(textAmplitudeFundamental.getText());
	            angFundamental = Double.parseDouble(textAnguloDeFase.getText());
	            
	            numeroHarmonicas = Integer.parseInt(comboBoxNumeroHarmonicas.getSelectedItem().toString());
	            
	    		
                    
	            ang1 = Double.parseDouble(textAngulo1.getText());
	            ang2 = Double.parseDouble(textAngulo2.getText());
	            ang3 = Double.parseDouble(textAngulo3.getText());
	            ang4 = Double.parseDouble(textAngulo4.getText());
	            ang5 = Double.parseDouble(textAngulo5.getText());
	            ang6 = Double.parseDouble(textAngulo6.getText());
	            
	            amp1 = Double.parseDouble(textAmplitude1.getText());
	            amp2= Double.parseDouble(textAmplitude2.getText());
	            amp3 = Double.parseDouble(textAmplitude3.getText());
	            amp4 = Double.parseDouble(textAmplitude4.getText());
	            amp5 = Double.parseDouble(textAmplitude5.getText());
	            amp6 = Double.parseDouble(textAmplitude6.getText());
	            
	            ord1 = Double.parseDouble(textOrdem1.getText());
	            ord2 = Double.parseDouble(textOrdem2.getText());
	            ord3 = Double.parseDouble(textOrdem3.getText());
	            ord4 = Double.parseDouble(textOrdem4.getText());
	            ord5 = Double.parseDouble(textOrdem5.getText());
	            ord6 = Double.parseDouble(textOrdem6.getText());
	                        
	            simulacao02.setAmplitudeHarmonico1(amp1);
	            simulacao02.setAmplitudeHarmonico2(amp2);
	            simulacao02.setAmplitudeHarmonico3(amp3);
	            simulacao02.setAmplitudeHarmonico4(amp4);
	            simulacao02.setAmplitudeHarmonico5(amp5);
	            simulacao02.setAmplitudeHarmonico6(amp6);
	            
	            simulacao02.setAnguloHarmonico1(ang1);
	            simulacao02.setAnguloHarmonico2(ang2);
	            simulacao02.setAnguloHarmonico3(ang3);
	            simulacao02.setAnguloHarmonico4(ang4);
	            simulacao02.setAnguloHarmonico5(ang5);
	            simulacao02.setAnguloHarmonico6(ang6);
	            
	            simulacao02.setOrdemHarmonico1(ord1);
	            simulacao02.setOrdemHarmonico2(ord2);
	            simulacao02.setOrdemHarmonico2(ord2);
	            simulacao02.setOrdemHarmonico3(ord3);
	            simulacao02.setOrdemHarmonico4(ord4);
	            simulacao02.setOrdemHarmonico5(ord5);
	            simulacao02.setOrdemHarmonico6(ord6);
	            
	            simulacao02.setAmplitudeComponenteFundamental(ampFundamental);
	            simulacao02.setAnguloComponenteFundamental(angFundamental);
	            

	            
	            //GRÁFICO COMPONENTE FUNDAMENTAL
	            ArrayList<Double> grafico4 = simulacao02.InserePontosComponenteFundamental();
	    		Grafico graficoComponenteFundamental = new Grafico(grafico4);
	    		graficoComponenteFundamental.setBounds(85,35,460,120);
	    		mainFrame1.getContentPane().add(graficoComponenteFundamental);
	    		
	          
	    		//GRÁFICO RESULTANTE
	    		ArrayList<Double> grafico5 = simulacao02.ondaResultante();
	    		Grafico graficoResultante = new Grafico(grafico5);
	    		graficoResultante.setBounds(85,560,400,83);
	    		mainFrame1.getContentPane().add(graficoResultante);
	    		
	    		//GRÀFICO ONDA HARMONICO 1
	    		ArrayList <Double> grafico6 = simulacao02.ondaHarmonico1();
	    		Grafico graficoHarmonico1 = new Grafico(grafico6);
	    		graficoHarmonico1.setBounds(85,190,400,40);
	    		mainFrame1.getContentPane().add(graficoHarmonico1);
	    		
	    		//GRÀFICO ONDA HARMONICO 2
	    		ArrayList <Double> grafico7 = simulacao02.ondaHarmonico2();
	    		Grafico graficoHarmonico2 = new Grafico(grafico7);
	    		graficoHarmonico2.setBounds(85,240,400,40);
	    		mainFrame1.getContentPane().add(graficoHarmonico2);
	    		
	    		//GRÀFICO ONDA HARMONICO 3
	    		ArrayList <Double> grafico8 = simulacao02.ondaHarmonico3();
	    		Grafico graficoHarmonico3 = new Grafico(grafico8);
	    		graficoHarmonico3.setBounds(85,290,400,40);
	    		mainFrame1.getContentPane().add(graficoHarmonico3);
	    		
	    		//GRÀFICO ONDA HARMONICO 4
	    		ArrayList <Double> grafico9 = simulacao02.ondaHarmonico4();
	    		Grafico graficoHarmonico4 = new Grafico(grafico9);
	    		graficoHarmonico4.setBounds(85,340,400,40);
	    		mainFrame1.getContentPane().add(graficoHarmonico4);
	    		
	    		//GRÀFICO ONDA HARMONICO 5
	    		ArrayList <Double> grafico10 = simulacao02.ondaHarmonico5();
	    		Grafico graficoHarmonico5 = new Grafico(grafico10);
	    		graficoHarmonico5.setBounds(85,390,400,40);
	    		mainFrame1.getContentPane().add(graficoHarmonico5);
	    		
	    		//GRÀFICO ONDA HARMONICO 6
	    		ArrayList <Double> grafico11 = simulacao02.ondaHarmonico6();
	    		Grafico graficoHarmonico6 = new Grafico(grafico11);
	    		graficoHarmonico6.setBounds(85,190,400,40);
	    		mainFrame1.getContentPane().add(graficoHarmonico6);
	    		
	    		
	    		
	    		
	            String comando = e.getActionCommand(); 
	            
	            	if(comando.equals("OK")){
	            	
	            	graficoComponenteFundamental.repaint();
	            	graficoComponenteFundamental.revalidate();
	            	
	            	
	          
	            	}
	            	else if (comando.equals("OK2") && numeroHarmonicas == 2){
	            		
	            		graficoResultante.repaint();
		            	graficoResultante.revalidate();
		            
		            	graficoHarmonico1.repaint();
		            	graficoHarmonico1.revalidate();
		            	
		            	
		            	graficoHarmonico2.repaint();
		            	graficoHarmonico2.revalidate();
		 
		            	}
	            	
	            	else if (comando.equals("OK2") && numeroHarmonicas == 3){
	            		
	            		graficoResultante.repaint();
		            	graficoResultante.revalidate();
		            
		            	graficoHarmonico1.repaint();
		            	graficoHarmonico1.revalidate();
		            	
		            	
		            	graficoHarmonico2.repaint();
		            	graficoHarmonico2.revalidate();
		 
		            	graficoHarmonico3.repaint();
		            	graficoHarmonico3.revalidate();
		            	           	
	            	}
	            	
	            	else if (comando.equals("OK2") && numeroHarmonicas == 4){
	            		
	            		graficoResultante.repaint();
		            	graficoResultante.revalidate();
		            
		            	graficoHarmonico1.repaint();
		            	graficoHarmonico1.revalidate();
		            	
		            	
		            	graficoHarmonico2.repaint();
		            	graficoHarmonico2.revalidate();
		 
		            	graficoHarmonico3.repaint();
		            	graficoHarmonico3.revalidate();
		            	
		            	graficoHarmonico4.repaint();
		            	graficoHarmonico4.revalidate();
		            	
		            	

	            }
	            	
	            	else if (comando.equals("OK2") && numeroHarmonicas == 5){
	            		
	            		graficoResultante.repaint();
		            	graficoResultante.revalidate();
		            
		            	graficoHarmonico1.repaint();
		            	graficoHarmonico1.revalidate();
		            	
		            	
		            	graficoHarmonico2.repaint();
		            	graficoHarmonico2.revalidate();
		 
		            	graficoHarmonico3.repaint();
		            	graficoHarmonico3.revalidate();
		            	
		            	graficoHarmonico4.repaint();
		            	graficoHarmonico4.revalidate();
		            	
		            	graficoHarmonico5.repaint();
		            	graficoHarmonico5.revalidate();
		            	
		            	graficoHarmonico6.repaint();
		            	graficoHarmonico6.revalidate();

	}

	            	
	            	else if (comando.equals("OK2") && numeroHarmonicas == 5){
	            		
	            		graficoResultante.repaint();
		            	graficoResultante.revalidate();
		            
		            	graficoHarmonico1.repaint();
		            	graficoHarmonico1.revalidate();
		            	
		            	
		            	graficoHarmonico2.repaint();
		            	graficoHarmonico2.revalidate();
		 
		            	graficoHarmonico3.repaint();
		            	graficoHarmonico3.revalidate();
		            	
		            	graficoHarmonico4.repaint();
		            	graficoHarmonico4.revalidate();
		            	
		            	graficoHarmonico5.repaint();
		            	graficoHarmonico5.revalidate();
		            	
		            	graficoHarmonico6.repaint();
		            	graficoHarmonico6.revalidate();
		
	}
	
}
		
		
}




