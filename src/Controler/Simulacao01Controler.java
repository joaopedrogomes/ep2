package Controler;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
//import javax.swing.JPanel;
import javax.swing.JTextField;

import Model.CalculoSimulacao01;
//import View.Simulacao01;
import View.Grafico;
import View.TrianguloPotencias;

public class Simulacao01Controler implements ActionListener 
{
	CalculoSimulacao01 simulacao01 = new CalculoSimulacao01();
	
	private JTextField textAnguloFi;
	private JFrame mainFrame;
	private JPanel trianguloDePotencia;
//	private Component rootPane;
	private JPanel graficos;
	private JTextField textAmplitudeTensao;
	private JTextField textAmplitudeCorrente;
	private JTextField textAnguloTensao;
	private JTextField textAnguloCorrente;
	private JTextField textPotenciaAtiva;
	private JTextField textPotenciaReativa;
	private JTextField textPotenciaAparente;
	private JTextField textFatorDePotencia;
	private Grafico graficoOndaTensao;
	private Grafico graficoOndaCorrente;
	private Grafico graficoPotenciaInstantanea;
//	private JPanel trianguloDePotencias;


	public Simulacao01Controler(
			
			JTextField textAnguloFi,
			JFrame mainFrame,
			JPanel trianguloDePotencia,
			JPanel graficos,
			JTextField textAmplitudeTensao,
			JTextField textAnguloTensao,
			JTextField textAmplitudeCorrente,
			JTextField textAnguloCorrente,
			JTextField textPotenciaAtiva,
			JTextField textPotenciaReativa,
			JTextField textPotenciaAparente,
			JTextField textFatorDePotencia) {
		
			this.textAnguloFi = textAnguloFi;
			this.mainFrame = mainFrame;
			this.trianguloDePotencia = trianguloDePotencia;
			this.graficos = graficos;
			this.textAmplitudeTensao = textAmplitudeTensao;
			this.textAnguloTensao = textAnguloTensao;
			this.textAmplitudeCorrente = textAmplitudeCorrente;
			this.textAnguloCorrente = textAnguloCorrente;
			this.textPotenciaAtiva = textPotenciaAtiva;
			this.textPotenciaReativa = textPotenciaReativa;
			this.textPotenciaAparente = textPotenciaAparente;
			this.textFatorDePotencia = textFatorDePotencia;
	
			
	}	
			

	public void actionPerformed(ActionEvent e){
		
		double AmpTensao, AmpCorrente;
//		double anguloFi;
		double AngTensao, AngCorrente;
		
//		String W = "W";
		
		try
		{
				AmpTensao = Double.parseDouble(textAmplitudeTensao.getText());
	            AngTensao = Double.parseDouble(textAnguloTensao.getText());
	            AmpCorrente = Double.parseDouble(textAmplitudeCorrente.getText());
	            AngCorrente = Double.parseDouble(textAnguloCorrente.getText());
	            
	           
	            simulacao01.setAmplitudeTensao(AmpTensao);
	            simulacao01.setAmplitudeCorrente(AmpCorrente);
	            simulacao01.setAnguloTensao(AngTensao);
	            simulacao01.setAnguloCorrente(AngCorrente);
	            
	            
	        	JTextField textAnguloFi = new JTextField();
	        	textAnguloFi.setBounds(568, 485, 40, 25);
	        	mainFrame.getContentPane().add(textAnguloFi);
	        	
	            TrianguloPotencias trianguloDePotencias = new TrianguloPotencias((simulacao01.PotenciaAtiva())/100,(simulacao01.PotenciaReativa())/100);
	            trianguloDePotencias.setBounds(695, 405, 250, 245);
	            mainFrame.getContentPane().add(trianguloDePotencias);
	            
	            ArrayList<Double> graficoTensao = simulacao01.InserePontosOndaTensao();
	            graficoOndaTensao = new Grafico(graficoTensao);
	            
	            graficoOndaTensao.setBounds(530,65,400,95);
	            mainFrame.getContentPane().add(graficoOndaTensao);
	            
	            ArrayList<Double> graficoCorrente = simulacao01.InserePontosOndaCorrente();
	            graficoOndaCorrente = new Grafico(graficoCorrente);
//	            
	            graficoOndaCorrente.setBounds(530,170,400,95);
	            mainFrame.getContentPane().add(graficoOndaCorrente);
	            
	            ArrayList<Double> graficoPotencia = simulacao01.InserePontosPotenciaInstantanea();
	            graficoPotenciaInstantanea = new Grafico(graficoPotencia);
	            
	            graficoPotenciaInstantanea.setBounds(530,275,400,95);
	            mainFrame.getContentPane().add(graficoPotenciaInstantanea);
	            
	            
	            String comando = e.getActionCommand(); 
	            
	            if(comando.equals("SIMULAR")){
	            	
	            	
	            	
	            	if(AmpTensao >= 0.0 && AmpTensao <=220.0 && AmpCorrente >= 0.0 && AmpCorrente <= 100 && AngTensao >= -180 && AngTensao <=180 && AngCorrente >= -180 && AngCorrente <= 180)
	            	{
	            		
	            	
	            		textPotenciaAtiva.setText(String.valueOf(simulacao01.PotenciaAtiva()));
	            		textPotenciaReativa.setText(String.valueOf(simulacao01.PotenciaReativa()) );
	            		textPotenciaAparente.setText(String.valueOf(simulacao01.PotenciaAparente()) );
	            		textFatorDePotencia.setText(String.valueOf(simulacao01.FatorPotencia()) );
	            		textAnguloFi.setText(String.valueOf(simulacao01.AnguloFi()));
	            		
	            		trianguloDePotencias.revalidate();
	            		trianguloDePotencias.repaint();
	            		
	            		graficoOndaTensao.revalidate();
	            		graficoOndaTensao.repaint();
	            		
	            		graficoOndaCorrente.revalidate();
	            		graficoOndaCorrente.repaint();
	            		
	            		graficoPotenciaInstantanea.revalidate();
	            		graficoPotenciaInstantanea.repaint();
//            
	            		textAnguloFi.revalidate();
	            		textAnguloFi.repaint();
	            	}
	            	
	            	else 
	            	{
	            		
						JOptionPane.showMessageDialog(null,"VALOR INVÀLIDO/INVALID VALUE");
	            		textPotenciaAtiva.setText("INVALID!");
	        			textPotenciaReativa.setText("INVALID!");
	        			textPotenciaAparente.setText("INVALID!");
	        			textFatorDePotencia.setText("INVALID!");
	            	}
	            }

		}
		catch(NumberFormatException exp)
		{
			textPotenciaAtiva.setText("ERROR!");
			textPotenciaReativa.setText("ERROR!");
			textPotenciaAparente.setText("ERROR!");
			textFatorDePotencia.setText("ERROR!");
		}
	}
	
}




