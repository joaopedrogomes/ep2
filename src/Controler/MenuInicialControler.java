package Controler;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import View.Simulacao01;
import View.Simulacao02;
import View.Simulacao03;

public class MenuInicialControler implements ActionListener {
	
	private Component rootPane;
	public MenuInicialControler(){}
	
	public void actionPerformed(ActionEvent e){
		String comando = e.getActionCommand();
		
		if (comando.equals("Fluxo de potência fundamental")){
			Simulacao01.main(null);
		}
		
		else if(comando.equals("Distorção harmônica")){
			
			Simulacao02.main(null);
		}
	
		else if(comando.equals("Fluxo de potência harmônico")){
			
			Simulacao03.main(null);
		}
		
		else{
			
			JOptionPane.showMessageDialog(rootPane,"NÃO IMPLEMENTADO");
		}
	}
	
	
}

