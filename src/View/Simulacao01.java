package View;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;

import Controler.Simulacao01Controler;
import Model.CalculoSimulacao01;


public class Simulacao01 {

	private static final String TITULO = "APRENDA QEE (QUALIDADE DA ENERGIA ELÉTRICA) - edition 1.0";
	private JFrame mainFrame;
	private JPanel graficos,entradasTensao,entradasCorrente;
	private JLabel lblEntradas;
	private JTextField textAmplitudeTensao;
	private JTextField textAmplitudeCorrente;
	private JTextField textAnguloTensao;
	private JTextField textAnguloCorrente;
	private JTextField textPotenciaAtiva;
	private JTextField textPotenciaReativa;
	private JTextField textPotenciaAparente;
	private JTextField textFatorDePotencia;
	private JButton btnSimular2;
	private JTextField textAnguloFi;

public Simulacao01(){}
	
	public static void main(String[] args){
	       
		   Simulacao01 interface1 = new Simulacao01();  
		   interface1.mainPanel();       
	   }
	
	private void mainPanel(){
		
		//FRAME PRINCIPAL
		mainFrame = new JFrame(TITULO);
		mainFrame.setSize(1200,700);
		mainFrame.getContentPane().setLayout(null);
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setVisible(true);
		
		//PANEL - ENTRADAS TENSÃO
		entradasTensao = new JPanel();                                            
		entradasTensao.setOpaque(false);
		entradasTensao.setLayout(null);
		entradasTensao.setBounds(12,60,427,108);
	    entradasTensao.setBorder(javax.swing.BorderFactory.createTitledBorder("TENSÃO:"));
	    
		//PANEL - ENTRADAS CORRENTE
		entradasCorrente = new JPanel();
		entradasCorrente.setOpaque(false);
		entradasCorrente.setLayout(null);
		entradasCorrente.setBounds(12,185, 427, 108);
	    entradasCorrente.setBorder(javax.swing.BorderFactory.createTitledBorder("CORRENTE"));
		
	    //PANEL - GRÁFICOS
		graficos = new JPanel();
		graficos.setOpaque(false);
		graficos.setLayout(null);
		graficos.setBounds(497,49,661,332);
		graficos.setBorder(javax.swing.BorderFactory.createTitledBorder("GRÁFICOS"));
		
		//PANEL - SAIDAS TENSÃO/CORRENTE
		JPanel saidas = new JPanel();
		saidas.setOpaque(false);
		saidas.setLayout(null);
		saidas.setBounds(12, 337, 435, 260);
		saidas.setBorder(javax.swing.BorderFactory.createTitledBorder("SAIDAS"));
	
			
		//PANEL - TRIANGULO DE POTENCIA
		JPanel trianguloDePotencia = new JPanel();
		trianguloDePotencia.setOpaque(false);
		trianguloDePotencia.setLayout(null);
		trianguloDePotencia.setBorder(javax.swing.BorderFactory.createTitledBorder("TRIÂNGULO DE POTÊNCIA"));
		trianguloDePotencia.setBounds(497, 393, 667, 262);
		
		
		// ADICIONANDO OS PANELS
		mainFrame.getContentPane().add(trianguloDePotencia);
		mainFrame.getContentPane().add(entradasTensao);
		mainFrame.getContentPane().add(entradasCorrente);
		mainFrame.getContentPane().add(graficos);
		mainFrame.getContentPane().add(saidas);
		
		//TRIANGULO DE POTÊNCIA
		TrianguloPotencias trianguloDePotencia1 = new TrianguloPotencias(-1,0);
        trianguloDePotencia1.setBounds(695, 405, 250, 245);
        mainFrame.getContentPane().add(trianguloDePotencia1);
		trianguloDePotencia1.repaint();
		
		//GRÁFICO POTENCIA INSTANTANEA
		CalculoSimulacao01 InserePontosPotenciaInstantanea = new CalculoSimulacao01();
		ArrayList<Double> grafico0 = InserePontosPotenciaInstantanea.InserePontosPotenciaInstantanea();
		
		Grafico graficoPotenciaInstantanea = new Grafico(grafico0);
		graficoPotenciaInstantanea.setBounds(530,275,400,95);
		mainFrame.getContentPane().add(graficoPotenciaInstantanea);
		graficoPotenciaInstantanea.repaint();
		
		//GRÁFICO TENSÃO 01
		CalculoSimulacao01 InserePontosOndaTensao = new CalculoSimulacao01(); //criando objeto graficotensao
		ArrayList<Double> grafico1 = InserePontosOndaTensao.InserePontosOndaTensao();
		
		Grafico graficoOndaTensao = new Grafico(grafico1);
		graficoOndaTensao.setBounds(530,65,400,95);
		mainFrame.getContentPane().add(graficoOndaTensao);
		graficoOndaTensao.repaint();
		
		//GRÁFICO CORRENTE 01
		
		CalculoSimulacao01 InserePontosOndaCorrente = new CalculoSimulacao01();
		ArrayList<Double> grafico2 = InserePontosOndaCorrente.InserePontosOndaCorrente();
		
		Grafico graficoOndaCorrente = new Grafico(grafico2);
		graficoOndaCorrente.setBounds(530,170,400,95);
		mainFrame.getContentPane().add(graficoOndaCorrente);
		graficoOndaCorrente.repaint();
		
		//LABEL SIMPLES ESCRITO "ENTRADAS"
		lblEntradas = new JLabel("ENTRADAS:");
		lblEntradas.setFont(new Font("Lato", Font.BOLD, 14));
		lblEntradas.setBounds(12, 34, 95, 15);
		mainFrame.getContentPane().add(lblEntradas);
		lblEntradas.repaint();
		
		//LABEL SIMPLES ESCRITO "AMPLITUDE DA TENSÃO"
		JLabel lblAmplitudeTensao = new JLabel("AMPLITUDE:");
		lblAmplitudeTensao.setForeground(new Color(0, 102, 153));
		lblAmplitudeTensao.setFont(new Font("Lato", Font.BOLD, 14));
		lblAmplitudeTensao.setBounds(17, 100, 200, 15);
		mainFrame.getContentPane().add(lblAmplitudeTensao);
		lblAmplitudeTensao.repaint();
		
		//LABEL SIMPLES ESCRITO "ÂNGULO"
		JLabel lblAnguloTensao = new JLabel("ÂNGULO:");
		lblAnguloTensao.setFont(new Font("Lato", Font.BOLD, 14));
		lblAnguloTensao.setForeground(new Color(0, 102, 153));
		lblAnguloTensao.setBounds(17,140,200,15);
		mainFrame.getContentPane().add(lblAnguloTensao);
		lblAnguloTensao.repaint();
		
		//TEXTFIELD AMPLITUDE TENSÃO
		textAmplitudeTensao = new JTextField();
		textAmplitudeTensao.setBounds(105, 95, 50, 25);
		mainFrame.getContentPane().add(textAmplitudeTensao);
		textAmplitudeTensao.setColumns(10);
		textAmplitudeTensao.repaint();
		
		//TEXTFIELD ANGULO TENSÃO
		textAnguloTensao = new JTextField();
		textAnguloTensao.setBounds(105,135,50,25);
		mainFrame.getContentPane().add(textAnguloTensao);
		textAmplitudeTensao.setColumns(10);
		textAnguloTensao.repaint();
		
		//LABEL SIMPLES ESCRITO "AMPLITUDE"
		JLabel lblAmplitudeCorrente = new JLabel("AMPLITUDE:");
		lblAmplitudeCorrente.setFont(new Font("Lato", Font.BOLD, 14));
		lblAmplitudeCorrente.setForeground(new Color(0, 102, 153));
		lblAmplitudeCorrente.setBounds(17, 220, 200, 15);
		mainFrame.getContentPane().add(lblAmplitudeCorrente);
		lblAmplitudeCorrente.repaint();
		
		//LABEL SIMPLES ESCRITO "ÂNGULO"
		JLabel lblAnguloCorrente = new JLabel("ÂNGULO:");
		lblAnguloCorrente.setFont(new Font("Lato", Font.BOLD, 14));
		lblAnguloCorrente.setForeground(new Color(0, 102, 153));
		lblAnguloCorrente.setBounds(17, 260, 200, 15);
		mainFrame.getContentPane().add(lblAnguloCorrente);
		lblAnguloCorrente.repaint();
		
		//TEXTFIELD AMPLITUDE CORRENTE
		textAmplitudeCorrente = new JTextField();
		textAmplitudeCorrente.setBounds(105, 215, 50, 25);
		mainFrame.getContentPane().add(textAmplitudeCorrente);
		textAmplitudeCorrente.setColumns(10);
		textAmplitudeCorrente.repaint();
		
		//TEXTFIELD ANGULO CORRENTE
		textAnguloCorrente = new JTextField();
		textAnguloCorrente.setBounds(105,255,50,25);
		mainFrame.getContentPane().add(textAnguloCorrente);
		textAmplitudeCorrente.setColumns(10);
		textAnguloCorrente.repaint();
		
		//LABEL SIMPLES ESCRITO "POTÊNCIA ATIVA:"(Watt)
		JLabel lblPotenciaAtiva = new JLabel("POTÊNCIA ATIVA:");
		lblPotenciaAtiva.setFont(new Font("Lato", Font.BOLD, 14));
		lblPotenciaAtiva.setForeground(new Color(0, 102, 153));
		lblPotenciaAtiva.setBounds(17, 370, 150, 15);
		mainFrame.getContentPane().add(lblPotenciaAtiva);
		lblPotenciaAtiva.repaint();
		
		//LABEL SIMPLES ESCRITO "POTÊNCIA REATIVA"(VAR)
		JLabel lblPotenciaReativa = new JLabel ("POTÊNCIA REATIVA:");
		lblPotenciaReativa.setFont(new Font("Lato", Font.BOLD,14));
		lblPotenciaReativa.setForeground(new Color(0,102,153));
		lblPotenciaReativa.setBounds(17,420,150,15);
		mainFrame.getContentPane().add(lblPotenciaReativa);
		lblPotenciaReativa.repaint();
		
		//LABEL SIMPLES ESCRITO "POTÊNCIA APARENTE" (VA)
		JLabel lblPotenciaAparente = new JLabel("POTÊNCIA APARENTE:");
		lblPotenciaAparente.setFont(new Font("Lato", Font.BOLD,14));
		lblPotenciaAparente.setForeground(new Color(0,102,153));
		lblPotenciaAparente.setBounds(17,470,150,15);
		mainFrame.getContentPane().add(lblPotenciaAparente);
		lblPotenciaAparente.repaint();
		
		//LABEL SIMPLES ESCRITO "FATOR DE POTÊNCIA"		
		JLabel lblFatorDePotencia = new JLabel("FATOR DE POTÊNCIA:");
		lblFatorDePotencia.setFont(new Font("Lato", Font.BOLD,14));
		lblFatorDePotencia.setForeground(new Color(0,102,153));
		lblFatorDePotencia.setBounds(17,520,150,15);
		mainFrame.getContentPane().add(lblFatorDePotencia);
		lblFatorDePotencia.repaint();
		
		//TEXTFIELD PARA POTÊNCIA ATIVA
		textPotenciaAtiva = new JTextField();
		textPotenciaAtiva.setBounds(170, 365, 50, 25);
		mainFrame.getContentPane().add(textPotenciaAtiva);
		textPotenciaAtiva.setColumns(10);
		textPotenciaAtiva.repaint();
		
		//TEXTFIELD PARA POTÊNCIA REATIVA
		textPotenciaReativa = new JTextField();
		textPotenciaReativa.setBounds(170, 415, 50, 25);
		mainFrame.getContentPane().add(textPotenciaReativa);
		textPotenciaReativa.setColumns(10);
		textPotenciaReativa.repaint();
		
		//TEXTFIELD PARA POTÊNCIA APARENTE
		textPotenciaAparente = new JTextField();
		textPotenciaAparente.setBounds(170, 465, 50, 25);
		mainFrame.getContentPane().add(textPotenciaAparente);
		textPotenciaAparente.setColumns(10);
		textPotenciaAparente.repaint();
		
		//TEXTFIELD PARA FATOR DE POTÊNCIA
		textFatorDePotencia = new JTextField();
		textFatorDePotencia.setBounds(170, 515, 50, 25);
		mainFrame.getContentPane().add(textFatorDePotencia);
		textFatorDePotencia.setColumns(10);
		textFatorDePotencia.repaint();
		
		//BOTÃO SIMULAR
		btnSimular2 = new JButton("SIMULAR");
		btnSimular2.setForeground(Color.RED);
		btnSimular2.setBounds(185, 305, 117, 25);
		mainFrame.getContentPane().add(btnSimular2);
		btnSimular2.repaint();

		btnSimular2.addActionListener(new Simulacao01Controler(
			
			
			textAnguloFi,
			mainFrame,
			trianguloDePotencia,
			graficos,
			textAmplitudeTensao,
			textAnguloTensao,
			textAmplitudeCorrente,
			textAnguloCorrente,
			textPotenciaAtiva,
			textPotenciaReativa,
			textPotenciaAparente, 
			textFatorDePotencia));
		
		//LEGENDA TRIANGULO DE POT
		JLabel lblAzul = new JLabel("___");
		lblAzul.setForeground(Color.BLUE);
		lblAzul.setFont(new Font("Dialog", Font.BOLD, 41));
		lblAzul.setBounds(480, 30, 70, 49);
		trianguloDePotencia.add(lblAzul);
		lblAzul.repaint();
		
		JLabel lblVerde = new JLabel("___");
		lblVerde.setForeground(Color.GREEN);
		lblVerde.setFont(new Font("Dialog", Font.BOLD, 41));
		lblVerde.setBounds(480, 90, 85, 52);
		trianguloDePotencia.add(lblVerde);
		lblVerde.repaint();
		
		JLabel lblVermelha = new JLabel("___");
		lblVermelha.setForeground(Color.RED);
		lblVermelha.setFont(new Font("Dialog", Font.BOLD, 41));
		lblVermelha.setBounds(480, 160, 63, 52);
		trianguloDePotencia.add(lblVermelha);
		lblVermelha.repaint();
		
		JLabel lblPotAparente = new JLabel("P.Aparente(VA)");
		lblPotAparente.setFont(new Font("Lato", Font.BOLD, 12));
		lblPotAparente.setBounds(555, 67, 100, 15);
		trianguloDePotencia.add(lblPotAparente);
		lblPotAparente.repaint();
		
		
		JLabel lblPreativavar = new JLabel("P.Reativa(var)");
		lblPreativavar.setFont(new Font("Lato", Font.BOLD, 12));
		lblPreativavar.setBounds(553, 124, 80, 25);
		trianguloDePotencia.add(lblPreativavar);
		lblPreativavar.repaint();
		
		JLabel lblPativawatt = new JLabel("P.Ativa(Watt)");
		lblPativawatt.setFont(new Font("Lato", Font.BOLD, 12));
		lblPativawatt.setBounds(553, 195, 80, 26);
		trianguloDePotencia.add(lblPativawatt);
		lblPativawatt.repaint();
		
		JLabel lblAnguloFi = new JLabel("Angulo Φ");
		lblAnguloFi.setFont(new Font("Lato", Font.BOLD, 15));
		lblAnguloFi.setBounds(57, 67, 75, 20);
		trianguloDePotencia.add(lblAnguloFi);
		lblAnguloFi.repaint();
		
		JLabel label = new JLabel("º");
		label.setFont(new Font("Dialog", Font.BOLD, 18));
		label.setBounds(120, 90, 20, 20);
		trianguloDePotencia.add(label);
		label.repaint();
		
		textAnguloFi = new JTextField();
		textAnguloFi.setBounds(568, 485, 40, 25);
		mainFrame.getContentPane().add(textAnguloFi);
		textAnguloFi.setColumns(10);
		textAnguloFi.repaint();
		
		//LABEL SIMPLES ESCRITO "FORMA DE ONDA TENSÃO" - AO LADO DO GRÁFICO
		JLabel lblOndaTensao = new JLabel("Forma de onda Tensão");
		lblOndaTensao.setFont(new Font("Lato", Font.BOLD, 14));
		lblOndaTensao.setBounds(456, 50, 193, 20);
		graficos.add(lblOndaTensao);
		lblOndaTensao.repaint();
		//FIM DA LEGENDA
		
		
		//LABEL SIMPLES ESCRITO "FORMA DE ONDA CORRENTE"- AO LADO DO GRÁFICO
		
		JLabel lblOndaCorrente = new JLabel("Forma de onda Corrente");
		lblOndaCorrente.setFont(new Font("Lato", Font.BOLD, 14));
		lblOndaCorrente.setBounds(456,150,193,20);
		graficos.add(lblOndaCorrente);
		lblOndaCorrente.repaint();
		
		JLabel lblFormaDeOnda = new JLabel("Forma de onda");
		lblFormaDeOnda.setFont(new Font("Lato", Font.BOLD, 14));
		lblFormaDeOnda.setBounds(456, 250, 193, 20);
		graficos.add(lblFormaDeOnda);
		lblFormaDeOnda.repaint();
		
		JLabel lblPotenciaInstantanea = new JLabel("Potencia Instantânea");
		lblPotenciaInstantanea.setFont(new Font("Lato", Font.BOLD, 14));
		lblPotenciaInstantanea.setBounds(455, 270, 130, 20);
		graficos.add(lblPotenciaInstantanea);
		lblPotenciaInstantanea.repaint();
		

		
		//LABEL SIMPLES ESCRITO "WATT"
		JLabel lblWatt = new JLabel("Watt");
		lblWatt.setFont(new Font("Lato", Font.BOLD, 12));
		lblWatt.setBounds(220, 375, 70, 15);
		mainFrame.getContentPane().add(lblWatt);
		lblWatt.repaint();
		
		//LABEL SIMPLES ESCRITO "VAR"
		JLabel lblvar = new JLabel("var");
		lblvar.setFont(new Font("Lato", Font.BOLD, 12));
		lblvar.setBounds(220, 425, 70, 15);
		mainFrame.getContentPane().add(lblvar);
		lblvar.repaint();
		
		//LABEL SIMPLES ESCRITO "VA"
		JLabel lblVA = new JLabel("VA");
		lblVA.setFont(new Font("Lato", Font.BOLD, 12));
		lblVA.setBounds(222, 475, 70, 15);
		mainFrame.getContentPane().add(lblVA);
		lblVA.repaint();
		
		
		
		
		
	}
}
