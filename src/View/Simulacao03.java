package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;

import Controler.Simulacao01Controler;
import javax.swing.UIManager;

public class Simulacao03 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel Simulacao01;
	private static final String TITULO = "APRENDA QEE (QUALIDADE DA ENERGIA ELÉTRICA) - edition 1.0";
	public static JTextField textAmpTensao;
	public static JTextField textAnguloDeFase;
	public static JTextField textAnguloDefasagem;
	private JTextField textField;
	private JTextField AmpSinalCorrente;
	private JTextField PotLiquida;
	private JTextField PotDistorcao;
	private JTextField FatorDePotVerdadeiro;
//	private JPanel panelGrafico;
 

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Simulacao03 frame = new Simulacao03();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Simulacao03() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle(TITULO);
		setBounds(700, 700,800,600);
		Simulacao01 = new JPanel();
		Simulacao01.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(Simulacao01);
		Simulacao01.setLayout(null);
		
		JLabel lblsubtitulo = new JLabel("Bem-vindo a simulação fluxo de potência fundamental");
		lblsubtitulo.setFont(new Font("Lato", Font.BOLD, 14));
		lblsubtitulo.setBounds(12, 12, 409, 26);
		Simulacao01.add(lblsubtitulo);
		
		JLabel lblEntradas = new JLabel("ENTRADAS:");
		lblEntradas.setFont(new Font("Lato", Font.BOLD, 15));
		lblEntradas.setBounds(12, 34, 92, 34);
		Simulacao01.add(lblEntradas);
		
		textAmpTensao = new JTextField();
		textAmpTensao.setBounds(175, 110, 50, 20);
		Simulacao01.add(textAmpTensao);
		textAmpTensao.setColumns(10);
		
		JLabel lblAngDeFase = new JLabel("Ângulo de fase:");
		lblAngDeFase.setFont(new Font("Lato", Font.BOLD, 14));
		lblAngDeFase.setBounds(12, 165, 117, 15);
		Simulacao01.add(lblAngDeFase);
		
		textAnguloDeFase = new JTextField();
		textAnguloDeFase.setBounds(110, 165, 50, 20);
		Simulacao01.add(textAnguloDeFase);
		textAnguloDeFase.setColumns(10);
		
		JLabel lblAmplitudeT = new JLabel("Sinal de Corrente");
		lblAmplitudeT.setForeground(new Color(0, 128, 0));
		lblAmplitudeT.setFont(new Font("Lato", Font.BOLD, 14));
		lblAmplitudeT.setBounds(12, 200, 200, 15);
		Simulacao01.add(lblAmplitudeT);
		
		JLabel lbAmplitudeT2 = new JLabel("Ordem harmônica");
		lbAmplitudeT2.setFont(new Font("Lato", Font.BOLD, 14));
		lbAmplitudeT2.setForeground(UIManager.getColor("Button.foreground"));
		lbAmplitudeT2.setBounds(12, 225, 139, 15);
		Simulacao01.add(lbAmplitudeT2);
		
		JLabel lblAnguloDefasagem = new JLabel("Ângulo de defasagem θ:");
		lblAnguloDefasagem.setFont(new Font("Lato", Font.BOLD, 14));
		lblAnguloDefasagem.setBounds(12, 265, 199, 15);
		Simulacao01.add(lblAnguloDefasagem);
		
		textAnguloDefasagem = new JTextField();
		textAnguloDefasagem.setColumns(10);
		textAnguloDefasagem.setBounds(170, 265, 50, 20);
		Simulacao01.add(textAnguloDefasagem);
		
		JLabel lblSaidas = new JLabel("SAIDAS:");
		lblSaidas.setFont(new Font("Lato", Font.BOLD, 15));
		lblSaidas.setBounds(12, 350, 92, 34);
		Simulacao01.add(lblSaidas);
		
//		JButton btnSimular = new JButton("Simular");
//		btnSimular.addActionListener(new Simulacao01Controler(AmpSinalCorrente, AmpSinalCorrente, AmpSinalCorrente, AmpSinalCorrente, AmpSinalCorrente, AmpSinalCorrente, AmpSinalCorrente, AmpSinalCorrente));
//		btnSimular.setBounds(12, 330, 117, 25);
//		Simulacao01.add(btnSimular);
//		
		JLabel lblSinaldeTensao = new JLabel("Sinal de Tensão");
		lblSinaldeTensao.setForeground(new Color(0, 128, 0));
		lblSinaldeTensao.setFont(new Font("Lato", Font.BOLD, 14));
		lblSinaldeTensao.setBounds(12, 63, 199, 15);
		Simulacao01.add(lblSinaldeTensao);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(145, 230, 50, 20);
		Simulacao01.add(textField);
		
		JLabel lblOrdem = new JLabel("Ordem");
		lblOrdem.setFont(new Font("Lato", Font.BOLD, 12));
		lblOrdem.setBounds(150, 215, 70, 15);
		Simulacao01.add(lblOrdem);
		
		JLabel lblAmplitudeTensao = new JLabel("Amplitude Tensão (Vrms):");
		lblAmplitudeTensao.setFont(new Font("Lato", Font.BOLD, 14));
		lblAmplitudeTensao.setBounds(12, 112, 199, 15);
		Simulacao01.add(lblAmplitudeTensao);
		
		JLabel lblDoSinalDa = new JLabel("do sinal da corrente:");
		lblDoSinalDa.setForeground(UIManager.getColor("Button.foreground"));
		lblDoSinalDa.setFont(new Font("Lato", Font.BOLD, 14));
		lblDoSinalDa.setBounds(12, 240, 139, 15);
		Simulacao01.add(lblDoSinalDa);
		
		JLabel lblAmplitudeDo = new JLabel("Amplitude do Sinal (Arms):");
		lblAmplitudeDo.setFont(new Font("Lato", Font.BOLD, 14));
		lblAmplitudeDo.setBounds(12, 300, 199, 15);
		Simulacao01.add(lblAmplitudeDo);
		
		AmpSinalCorrente = new JTextField();
		AmpSinalCorrente.setColumns(10);
		AmpSinalCorrente.setBounds(180, 300, 50, 20);
		Simulacao01.add(AmpSinalCorrente);
		
		JLabel lblNewLabel = new JLabel("Valor da potência liquida:");
		lblNewLabel.setFont(new Font("Lato", Font.BOLD, 14));
		lblNewLabel.setBounds(12, 380, 167, 15);
		Simulacao01.add(lblNewLabel);
		
		PotLiquida = new JTextField();
		PotLiquida.setColumns(10);
		PotLiquida.setBounds(180, 379, 50, 20);
		Simulacao01.add(PotLiquida);
		
		JLabel lblPotDistorcao = new JLabel("Valor da potência de distorção:");
		lblPotDistorcao.setFont(new Font("Lato", Font.BOLD, 14));
		lblPotDistorcao.setBounds(12, 420, 200, 15);
		Simulacao01.add(lblPotDistorcao);
		
		PotDistorcao = new JTextField();
		PotDistorcao.setColumns(10);
		PotDistorcao.setBounds(210, 419, 50, 20);
		Simulacao01.add(PotDistorcao);
		
		JLabel lblFatorDePotncia = new JLabel("Fator de potência verdadeiro (TPF):");
		lblFatorDePotncia.setBounds(12, 475, 277, 15);
		Simulacao01.add(lblFatorDePotncia);
		
		FatorDePotVerdadeiro = new JTextField();
		FatorDePotVerdadeiro.setBounds(266, 473, 50, 19);
		Simulacao01.add(FatorDePotVerdadeiro);
		FatorDePotVerdadeiro.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 12, 342, 549);
		Simulacao01.add(panel);
	}
}
