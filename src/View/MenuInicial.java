package View;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import Controler.MenuInicialControler;

public class MenuInicial extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private static final String TITULO = "APRENDA QEE (QUALIDADE DA ENERGIA ELÉTRICA) - edition 1.0";
	
	private JPanel JanelaPrincipal;
	private JLabel titulo;

/*---------------------------- INTERFACE PRINCIPAL ----------------------------------------------------------------*/
	
	public MenuInicial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle(TITULO);
		setBounds(700, 700,800,600);
		JanelaPrincipal = new JPanel();
		JanelaPrincipal.setBackground(Color.LIGHT_GRAY);
		JanelaPrincipal.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(JanelaPrincipal);
		JanelaPrincipal.setLayout(null);
		
		JLabel lblsubtitulo = new JLabel("   Programa didático de Quaidade de Energia - Edição 1.0");
		lblsubtitulo.setFont(new Font("Lato", Font.BOLD, 14));
		lblsubtitulo.setBounds(0, 12, 409, 26);
		JanelaPrincipal.add(lblsubtitulo);
		
		JLabel lblEscolhaASimulao = new JLabel("   Escolha a simulação que deseja: ");
		lblEscolhaASimulao.setForeground(new Color(34, 139, 34));
		lblEscolhaASimulao.setFont(new Font("Lato", Font.BOLD, 15));
		lblEscolhaASimulao.setBounds(0, 62, 364, 26);
		JanelaPrincipal.add(lblEscolhaASimulao);
		
		/*---------------------------- BOTAO 01 ----------------------------------------------------------------*/
		
		JButton btnsimulacao1 = new JButton("Fluxo de potência fundamental");
		btnsimulacao1.setActionCommand("Fluxo de potência fundamental");
		btnsimulacao1.addActionListener(new MenuInicialControler());
		
		btnsimulacao1.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao1.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao1.setBounds(15, 113, 321, 35);
		JanelaPrincipal.add(btnsimulacao1);
		
		/*---------------------------- BOTAO 02 ----------------------------------------------------------------*/
		
		JButton btnsimulacao2 = new JButton("Distorção harmônica");
		btnsimulacao2.setActionCommand("Distorção harmônica");
		btnsimulacao2.addActionListener(new MenuInicialControler());		
		
		btnsimulacao2.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao2.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao2.setBounds(15, 173, 321, 35);
		JanelaPrincipal.add(btnsimulacao2);
		
		/*---------------------------- BOTAO 03 ----------------------------------------------------------------*/
		
		JButton btnsimulacao3 = new JButton("Fluxo de potência harmônico");
		btnsimulacao3.setActionCommand("Fluxo de potência harmônico");
		btnsimulacao3.addActionListener(new MenuInicialControler());		

		btnsimulacao3.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao3.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao3.setBounds(15, 233, 321, 35);
		JanelaPrincipal.add(btnsimulacao3);

		/*---------------------------- BOTAO 04 ----------------------------------------------------------------*/
		
		JButton btnsimulacao4 = new JButton("Sequência de fases dos harmônicos");
		btnsimulacao4.setActionCommand("Sequência de fases dos hamônicos");
		btnsimulacao4.addActionListener(new MenuInicialControler());	

		
		btnsimulacao4.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao4.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao4.setBounds(15, 353, 321, 35);
		JanelaPrincipal.add(btnsimulacao4);

		/*---------------------------- BOTAO 05 ----------------------------------------------------------------*/
		
		JButton btnsimulacao5 = new JButton("Sequência de vetores fundamentais");
		btnsimulacao5.setActionCommand("Sequência de vetores fundamentais");
		btnsimulacao5.addActionListener(new MenuInicialControler());		

		btnsimulacao5.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao5.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao5.setBounds(15, 293, 321, 35);
		JanelaPrincipal.add(btnsimulacao5);
		
		/*---------------------------- BOTAO 06 ----------------------------------------------------------------*/
		
		JButton btnsimulacao6 = new JButton("Efeito flicker");
		btnsimulacao6.setActionCommand("Efeito flicker");
		btnsimulacao6.addActionListener(new MenuInicialControler());		

		
		btnsimulacao6.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao6.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao6.setBounds(15, 413, 321, 35);
		JanelaPrincipal.add(btnsimulacao6);

		/*---------------------------- BOTAO 07 ----------------------------------------------------------------*/
		
		JButton btnsimulacao7 = new JButton("Efeitos de afundamento de tensão");
		btnsimulacao7.setActionCommand("Efeitos de afundamento de tensão");
		btnsimulacao7.addActionListener(new MenuInicialControler());		

		btnsimulacao7.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao7.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao7.setBounds(400, 113, 321, 35);
		JanelaPrincipal.add(btnsimulacao7);
		
		/*---------------------------- BOTAO 08 ----------------------------------------------------------------*/
		
		JButton btnsimulacao8 = new JButton("Fonte de impedância de distorção");
		btnsimulacao8.setActionCommand("Fonte de impedância de distorção");
		btnsimulacao8.addActionListener(new MenuInicialControler());		

		
		btnsimulacao8.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao8.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao8.setBounds(400, 173, 321, 35);
		JanelaPrincipal.add(btnsimulacao8);
		
		/*---------------------------- BOTAO 09 ----------------------------------------------------------------*/

		JButton btnsimulacao9 = new JButton("Fator de potência");
		btnsimulacao9.setActionCommand("Fator de potência");
		btnsimulacao9.addActionListener(new MenuInicialControler());		
		
		btnsimulacao9.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao9.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao9.setBounds(400, 233, 321, 35);
		JanelaPrincipal.add(btnsimulacao9);
		/*---------------------------- BOTAO 10 ----------------------------------------------------------------*/
		
		JButton btnsimulacao10 = new JButton("Resposta em frequência");
		btnsimulacao10.setActionCommand("Resposta em frequência");
		btnsimulacao10.addActionListener(new MenuInicialControler());		

		btnsimulacao10.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao10.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao10.setBounds(400, 293, 321, 35);
		JanelaPrincipal.add(btnsimulacao10);
		
		/*---------------------------- BOTAO 11 ----------------------------------------------------------------*/
		
		JButton btnsimulacao11 = new JButton("Filtros");
		btnsimulacao11.setActionCommand("Filtros");
		btnsimulacao11.addActionListener(new MenuInicialControler());		

		btnsimulacao11.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao11.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao11.setBounds(400, 353, 321, 35);
		JanelaPrincipal.add(btnsimulacao11);

		
		/*---------------------------- BOTAO 12 ----------------------------------------------------------------*/
	
		JButton btnsimulacao12 = new JButton("Causas do afundamento de tensão");
		btnsimulacao12.setActionCommand("Causas do afundamento de tensão");
		btnsimulacao12.addActionListener(new MenuInicialControler());		

		
		btnsimulacao12.setIcon(new ImageIcon(MenuInicial.class.getResource("/javax/swing/plaf/metal/icons/ocean/maximize-pressed.gif")));
		btnsimulacao12.setFont(new Font("Lato", Font.BOLD, 14));
		btnsimulacao12.setBounds(400, 403, 321, 35);
		JanelaPrincipal.add(btnsimulacao12);
		
		titulo = new JLabel("",JLabel.LEFT);

		
	}
	
	public void mostrarJanela ()
	
	{
		titulo.setText(TITULO);
		titulo.setFont(new Font ("Dialog", Font.PLAIN, 40));
		
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					MenuInicial frame = new MenuInicial();
					
					frame.setVisible(true);
				
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}




