package View;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;

import Controler.Simulacao01Controler;
import Controler.Simulacao02Controler;
import Model.CalculoSimulacao01;
import Model.CalculoSimulacao02;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JComboBox;

public class Simulacao02 {


	
	private static final String TITULO = "APRENDA QEE (QUALIDADE DA ENERGIA ELÉTRICA) - edition 1.0";
	private JFrame mainFrame1;
	private JPanel componenteFundamental,entradasComponenteFundamental1;
	private JPanel panelSaidas;
	private JTextField textAmplitudeFundamental;
	private JTextField textAnguloDeFase;
	private JLabel lblNumeroOrdensHarmonicas;
	private JLabel lblHarmonicos;
	private JLabel lblAmplitude1;
	private JTextField textAmplitude1;
	private JLabel lblOrdem1;
	private JTextField textOrdem1;
	private JLabel lblAngulo1;
	private JTextField textAngulo1;
	private JLabel lblAmplitude2;
	private JTextField textAmplitude2;
	private JLabel lblOrdem2;
	private JTextField textOrdem2;
	private JLabel lblAngulo2;
	private JTextField textAngulo2;
	private JLabel lblAmplitude3;
	private JLabel lblAmplitude5;
	private JTextField textAmplitude3;
	private JTextField textAmplitude5;
	private JLabel lblOrdem3;
	private JTextField textOrdem3;
	private JLabel lblAngulo3;
	private JTextField textAngulo3;
	private JLabel lblAmplitude4;
	private JTextField textAmplitude4;
	private JLabel lblOrdem4;
	private JTextField textOrdem4;
	private JLabel lblAngulo4;
	private JTextField textAngulo4;
	private JLabel lblOrdem5;
	private JTextField textOrdem5;
	private JLabel lblAngulo5;
	private JTextField textAngulo5;
	private JLabel lblAmplitude6;
	private JTextField textAmplitude6;
	private JLabel lblOrdem6;
	private JTextField textOrdem6;
	private JLabel lblAngulo6;
	private JTextField textAngulo6;
	private JButton btnOk2;
	private JLabel lblResultante;
	

public Simulacao02(){}
	
	public static void main(String[] args){
	       
		   Simulacao02 interface2 = new Simulacao02();  
		   interface2.mainPanel2();       
	   }
	
	private void mainPanel2(){
		
		//FRAME PRINCIPAL
		mainFrame1 = new JFrame(TITULO);
		mainFrame1.setSize(1200,700);
		mainFrame1.getContentPane().setLayout(null);
		mainFrame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame1.setVisible(true);
		
		//PANEL - 
//		FORMA DE ONDA COMPONENTE FUNDAMENTAL
		componenteFundamental = new JPanel();                                            
		componenteFundamental.setOpaque(false);
		componenteFundamental.setLayout(null);
		componenteFundamental.setBounds(50,10,535,157);
		componenteFundamental.setBorder(javax.swing.BorderFactory.createTitledBorder("COMPONENTE FUNDAMENTAL"));
	    
		//PANEL - ENTRADAS COMPONENTE FUNDAMENTAL
		entradasComponenteFundamental1 = new JPanel();
		entradasComponenteFundamental1.setOpaque(false);
		entradasComponenteFundamental1.setLayout(null);
		entradasComponenteFundamental1.setBounds(597,10, 579, 157);
	    entradasComponenteFundamental1.setBorder(javax.swing.BorderFactory.createTitledBorder("ENTRADAS:"));
		
		//PANEL - HARMONICAS GRÁFICOS
		JPanel panelHarmonicas = new JPanel();
		panelHarmonicas.setOpaque(false);
		panelHarmonicas.setLayout(null);
		panelHarmonicas.setBounds(50, 170, 535, 337);
		panelHarmonicas.setBorder(javax.swing.BorderFactory.createTitledBorder("HARMÔNICOS"));
		
		//PANEL ENTRADAS HARMONICAS
		JPanel panelEntradasHarmonicas = new JPanel();
		panelEntradasHarmonicas.setLayout(null);
		panelEntradasHarmonicas.setOpaque(false);
		panelEntradasHarmonicas.setBorder(javax.swing.BorderFactory.createTitledBorder("ENTRADAS"));
		panelEntradasHarmonicas.setBounds(598, 170, 578, 300);
	
	
		//PANEL - SAIDAS
		panelSaidas = new JPanel();
		panelSaidas.setLayout(null);
		panelSaidas.setOpaque(false);
		panelSaidas.setBorder(javax.swing.BorderFactory.createTitledBorder("SAIDAS:"));
		panelSaidas.setBounds(50, 505, 1127, 157);
			
			
		
		// ADICIONANDO OS PANELS
		mainFrame1.getContentPane().add(componenteFundamental);
		mainFrame1.getContentPane().add(entradasComponenteFundamental1);
		mainFrame1.getContentPane().add(panelHarmonicas);
		mainFrame1.getContentPane().add(panelSaidas);
		
		//LABEL SÉRIE DE FURIER
		JLabel lblSerieDeFurier = new JLabel("SÉRIE DE FOURIER AMPLITUDE-FASE:");
		lblSerieDeFurier.setForeground(new Color(0, 102, 153));
		lblSerieDeFurier.setFont(new Font("Lato", Font.BOLD, 18));
		lblSerieDeFurier.setBounds(700, 25, 340, 20);
		panelSaidas.add(lblSerieDeFurier);
		lblSerieDeFurier.repaint();
		
		//LABEL RESULTANTE
		lblResultante = new JLabel("RESULTANTE:");
		lblResultante.setForeground(new Color(0, 102, 153));
		lblResultante.setFont(new Font("Lato", Font.BOLD, 18));
		lblResultante.setBounds(100, 25, 120, 20);
		panelSaidas.add(lblResultante);
		mainFrame1.getContentPane().add(panelEntradasHarmonicas);
	
		//LABEL SIMPLES ESCRITO: "Nº de ordens harmônicas:"
		lblNumeroOrdensHarmonicas = new JLabel("Nº DE ORDENS HARMÔNICAS:");
		lblNumeroOrdensHarmonicas.setForeground(new Color(0, 102, 153));
		lblNumeroOrdensHarmonicas.setFont(new Font("Lato", Font.BOLD, 18));
		lblNumeroOrdensHarmonicas.setBounds(12, 20, 270, 22);
		panelEntradasHarmonicas.add(lblNumeroOrdensHarmonicas);
		
		
		// RADIO BUTTON SELEÇÃO IMPAR
		JRadioButton rdbtnImpares = new JRadioButton("Ímpares");
		rdbtnImpares.setBounds(305, 45, 90, 23);
		panelEntradasHarmonicas.add(rdbtnImpares);
		rdbtnImpares.repaint();
		
		//RADIO BUTTON SELEÇÃO PAR
		JRadioButton rdbtnPares = new JRadioButton("Pares");
		rdbtnPares.setBounds(424, 45, 90, 23);
		panelEntradasHarmonicas.add(rdbtnPares);
		rdbtnPares.repaint();
		
		//LABEL SIMPLES ESCRITO "HARMÔNICOS"
		lblHarmonicos = new JLabel("HARMÔNICOS:");
		lblHarmonicos.setForeground(new Color(0, 102, 153));
		lblHarmonicos.setFont(new Font("Lato", Font.BOLD, 18));
		lblHarmonicos.setBounds(340, 20, 135, 20);
		panelEntradasHarmonicas.add(lblHarmonicos);
		lblHarmonicos.repaint();
		
		
		//LABEL SIMPLES ESCRITO AMPLITUDE - amplitude "1"
		lblAmplitude1 = new JLabel("Amplitude:");
		lblAmplitude1.setBounds(5, 100, 80, 15);
		panelEntradasHarmonicas.add(lblAmplitude1);
		lblAmplitude1.repaint();
		
		//TEXFIELD AMPLITUDE "1"
		textAmplitude1 = new JTextField();
		textAmplitude1.setBounds(80, 95, 35, 25);
		panelEntradasHarmonicas.add(textAmplitude1);
		textAmplitude1.setColumns(10);
		textAmplitude1.repaint();
		
		//LABEL SIMPLES ORDEM "1"
		lblOrdem1 = new JLabel("Ordem:");
		lblOrdem1.setBounds(115, 100, 70, 15);
		panelEntradasHarmonicas.add(lblOrdem1);
		lblOrdem1.repaint();
		
		//TEXFIELD ORDEM "1"
		textOrdem1 = new JTextField();
		textOrdem1.setBounds(168, 95, 25, 25);
		panelEntradasHarmonicas.add(textOrdem1);
		textOrdem1.setColumns(10);
		textOrdem1.repaint();
		
		//LABEL SIMPLES ESCRITO ÂNGULO "1"
		lblAngulo1 = new JLabel("Ângulo:");
		lblAngulo1.setBounds(195, 100, 70, 15);
		panelEntradasHarmonicas.add(lblAngulo1);
		lblAngulo1.repaint();
		
		//TEXTFIELD ANGULO"1"
		textAngulo1 = new JTextField();
		textAngulo1.setColumns(10);
		textAngulo1.setBounds(250, 95, 25, 25);
		panelEntradasHarmonicas.add(textAngulo1);
		textAngulo1.repaint();
		
		// LABEL SIMPLES ESCRITO "AMPLITUDE"
		lblAmplitude2 = new JLabel("Amplitude:");
		lblAmplitude2.setBounds(276, 100, 80, 15);
		panelEntradasHarmonicas.add(lblAmplitude2);
		lblAmplitude2.repaint();
		
		//TEXTFIELD AMPLITUDE "2"
		textAmplitude2 = new JTextField();
		textAmplitude2.setColumns(10);
		textAmplitude2.setBounds(352, 95, 35, 25);
		panelEntradasHarmonicas.add(textAmplitude2);
		textAmplitude2.repaint();
		
		//LABEL SIMPLES ESCRITO ORDEM "2"
		lblOrdem2 = new JLabel("Ordem:");
		lblOrdem2.setBounds(394, 100, 70, 15);
		panelEntradasHarmonicas.add(lblOrdem2);
		lblOrdem2.repaint();
		
		//TEXFIELD ORDEM "2"
		textOrdem2 = new JTextField();
		textOrdem2.setColumns(10);
		textOrdem2.setBounds(450, 95, 25, 25);
		panelEntradasHarmonicas.add(textOrdem2);
		textOrdem2.repaint();
		
		//LABEL SIMPLES ESCRITO ANGULO "2"
		lblAngulo2 = new JLabel("Ângulo:");
		lblAngulo2.setBounds(479, 100, 70, 15);
		panelEntradasHarmonicas.add(lblAngulo2);
		lblAngulo2.repaint();
		
		//TEXTFIELD ANGULO "2"
		textAngulo2 = new JTextField();
		textAngulo2.setColumns(10);
		textAngulo2.setBounds(541, 95, 25, 25);
		panelEntradasHarmonicas.add(textAngulo2);
		textAngulo2.repaint();
		
		//LABEL SIMPLES ESCRITO AMPLITUDE "3"
		lblAmplitude3 = new JLabel("Amplitude:");
		lblAmplitude3.setBounds(5, 165, 80, 15);
		panelEntradasHarmonicas.add(lblAmplitude3);
//		lblAmplitude3.setVisible(false);
		lblAmplitude3.repaint();
		
		//TEXFIELD AMPLITUDE "3"
		textAmplitude3 = new JTextField();
		textAmplitude3.setColumns(10);
		textAmplitude3.setBounds(80, 160, 35, 25);
		panelEntradasHarmonicas.add(textAmplitude3);
		textAmplitude3.repaint();
		
		// LABEL SIMPLES AMPLITUDE "5"
		lblAmplitude5 = new JLabel("Amplitude:");
		lblAmplitude5.setBounds(5, 230, 80, 15);
		panelEntradasHarmonicas.add(lblAmplitude5);
		lblAmplitude5.repaint();
		
		//TEXFIELD AMPLITUDE "5"
		textAmplitude5 = new JTextField();
		textAmplitude5.setColumns(10);
		textAmplitude5.setBounds(80, 225, 35, 25);
		panelEntradasHarmonicas.add(textAmplitude5);
		textAmplitude5.repaint();
		
		//LABEL SIMPLES ORDEM "3"
		lblOrdem3 = new JLabel("Ordem:");
		lblOrdem3.setBounds(115, 165, 70, 15);
		panelEntradasHarmonicas.add(lblOrdem3);
		lblOrdem3.repaint();
		
		//TEXTFIELD ORDEM "3"
		textOrdem3 = new JTextField();
		textOrdem3.setColumns(10);
		textOrdem3.setBounds(168, 160, 25, 25);
		panelEntradasHarmonicas.add(textOrdem3);
		textOrdem3.repaint();
		
		
		//LABEL SIMPLES ANGULO "3"
		lblAngulo3 = new JLabel("Ângulo:");
		lblAngulo3.setBounds(195, 165, 70, 15);
		panelEntradasHarmonicas.add(lblAngulo3);
		lblAngulo3.repaint();
		
		//TEXTFIELD ANGULO "3"
		textAngulo3 = new JTextField();
		textAngulo3.setColumns(10);
		textAngulo3.setBounds(250, 160, 25, 25);
		panelEntradasHarmonicas.add(textAngulo3);
		textAngulo3.repaint();
		
		//LABEL SIMPLES AMPLITUDE "4"
		lblAmplitude4 = new JLabel("Amplitude:");
		lblAmplitude4.setBounds(276, 165, 80, 15);
		panelEntradasHarmonicas.add(lblAmplitude4);
		lblAmplitude4.repaint();
	
		//TEXFIELD AMPLITUDE "4"
		textAmplitude4 = new JTextField();
		textAmplitude4.setColumns(10);
		textAmplitude4.setBounds(351, 160, 35, 25);
		panelEntradasHarmonicas.add(textAmplitude4);
		textAmplitude4.repaint();
		
		//LABEL SIMPLES ORDEM "4"
		lblOrdem4 = new JLabel("Ordem:");
		lblOrdem4.setBounds(394, 165, 70, 15);
		panelEntradasHarmonicas.add(lblOrdem4);
		lblOrdem4.repaint();
		
		//TEXFIELD ORDEM "4"
		textOrdem4 = new JTextField();
		textOrdem4.setColumns(10);
		textOrdem4.setBounds(450, 160, 25, 25);
		panelEntradasHarmonicas.add(textOrdem4);
		textOrdem4.repaint();
		
		//LABEL SIMPLES ANGULO "4"
		lblAngulo4 = new JLabel("Ângulo:");
		lblAngulo4.setBounds(479, 165, 70, 15);
		panelEntradasHarmonicas.add(lblAngulo4);
		lblAngulo4.repaint();
		
		//TEXTFIELD ANGULO "4"
		textAngulo4 = new JTextField();
		textAngulo4.setColumns(10);
		textAngulo4.setBounds(541, 160, 25, 25);
		panelEntradasHarmonicas.add(textAngulo4);
		textAngulo4.repaint();
		
		//LABEL SIMPLES ESCRITO ORDEM "5"
		lblOrdem5 = new JLabel("Ordem:");
		lblOrdem5.setBounds(116, 230, 70, 15);
		panelEntradasHarmonicas.add(lblOrdem5);
		lblOrdem5.repaint();
		
		//TEXTFIELD ORDEM "5"
		textOrdem5 = new JTextField();
		textOrdem5.setColumns(10);
		textOrdem5.setBounds(170, 225, 25, 25);
		panelEntradasHarmonicas.add(textOrdem5);
		textOrdem5.repaint();
		
		//LABEL SIMPLES ESCRITO ANGULO "5"
		lblAngulo5 = new JLabel("Ângulo:");
		lblAngulo5.setBounds(195, 230, 70, 15);
		panelEntradasHarmonicas.add(lblAngulo5);
		lblAngulo5.repaint();
		
		//TEXTFIELD ANGULO "5"
		textAngulo5 = new JTextField();
		textAngulo5.setColumns(10);
		textAngulo5.setBounds(250, 225, 25, 25);
		panelEntradasHarmonicas.add(textAngulo5);
		textAngulo5.repaint();
		
		
		//LABEL SIMPLES AMPLITUDE "6"
		lblAmplitude6 = new JLabel("Amplitude:");
		lblAmplitude6.setBounds(276, 230, 80, 15);
		panelEntradasHarmonicas.add(lblAmplitude6);
		lblAmplitude6.repaint();
		
		//TEXTFIELD AMPLITUDE "6"
		textAmplitude6 = new JTextField();
		textAmplitude6.setColumns(10);
		textAmplitude6.setBounds(352, 225, 35, 25);
		panelEntradasHarmonicas.add(textAmplitude6);
		textAmplitude6.repaint();
		
		//LABEL SIMPLES ORDEM "6'
		lblOrdem6 = new JLabel("Ordem:");
		lblOrdem6.setBounds(394, 230, 70, 15);
		panelEntradasHarmonicas.add(lblOrdem6);
		lblOrdem6.repaint();
		
		// TEXTFIELD ORDEM '6'
		textOrdem6 = new JTextField();
		textOrdem6.setColumns(10);
		textOrdem6.setBounds(450, 225, 25, 25);
		panelEntradasHarmonicas.add(textOrdem6);
		textOrdem6.repaint();
		
		//LABEL SIMPLES ANGULO '6'
		lblAngulo6 = new JLabel("Ângulo:");
		lblAngulo6.setBounds(479, 230, 70, 15);
		panelEntradasHarmonicas.add(lblAngulo6);
		lblAngulo6.repaint();
		
		//TEXTFIELD ANGULO '6'
		textAngulo6 = new JTextField();
		textAngulo6.setColumns(10);
		textAngulo6.setBounds(541, 225, 25, 25);
		panelEntradasHarmonicas.add(textAngulo6);
		textAngulo6.repaint();
		
	
		//TEXFIELD PARA AMPLITUDE FUNDAMENTAL
		textAmplitudeFundamental = new JTextField();
		textAmplitudeFundamental.setBounds(140, 70, 50, 25);
		entradasComponenteFundamental1.add(textAmplitudeFundamental);
		textAmplitudeFundamental.setColumns(10);
		textAmplitudeFundamental.repaint();
		
		//LABEL SIMPLES ESCRITO AMPLITUDE:
		JLabel lblAmplitudeFundamental = new JLabel("AMPLITUDE:");
		lblAmplitudeFundamental.setForeground(new Color(0, 102, 153));
		lblAmplitudeFundamental.setFont(new Font("Lato", Font.BOLD, 18));
		lblAmplitudeFundamental.setBounds(20, 75, 120, 20);
		entradasComponenteFundamental1.add(lblAmplitudeFundamental);
		lblAmplitudeFundamental.repaint();
		
		//TEXTFIELD ANGULO DE FASE
		textAnguloDeFase = new JTextField();
		textAnguloDeFase.setColumns(10);
		textAnguloDeFase.setBounds(422, 70, 50, 25);
		entradasComponenteFundamental1.add(textAnguloDeFase);
		textAnguloDeFase.repaint();
		
		//LABEL SIMPLES ESCRITO ANGULO DE FASE
		JLabel lblAnguloDeFase = new JLabel("ANGULO DE FASE (θº):");
		lblAnguloDeFase.setForeground(new Color(0, 102, 153));
		lblAnguloDeFase.setFont(new Font("Lato", Font.BOLD, 18));
		lblAnguloDeFase.setBounds(225, 75, 190, 20);
		entradasComponenteFundamental1.add(lblAnguloDeFase);
		lblAnguloDeFase.repaint();
		
		//GRÁFICO COMPONENTE FUNDAMENTAL
		CalculoSimulacao02 InserePontosComponenteFundamental = new CalculoSimulacao02();
		ArrayList<Double> grafico4 = InserePontosComponenteFundamental.InserePontosComponenteFundamental();
		Grafico graficoComponenteFundamental = new Grafico(grafico4);
		graficoComponenteFundamental.setBounds(85,35,460,120);
		mainFrame1.getContentPane().add(graficoComponenteFundamental);
		graficoComponenteFundamental.repaint();

		//GRÁFICO RESULTANTE
		CalculoSimulacao02 OndaResutante = new CalculoSimulacao02();
		ArrayList<Double> grafico5 = OndaResutante.ondaResultante();
		Grafico graficoResultante = new Grafico(grafico5);
		graficoResultante.setBounds(85,560,400,83);
		mainFrame1.getContentPane().add(graficoResultante);
		graficoResultante.repaint();
		
		//GRÁFICO ONDA HARMONICO 1
		CalculoSimulacao02 ondaHarmonico1 = new CalculoSimulacao02();
		ArrayList<Double> grafico6 = ondaHarmonico1.ondaHarmonico1();
		Grafico graficoHarmonico1 = new Grafico(grafico6);
		graficoHarmonico1.setBounds(85,190,400,40);
		mainFrame1.getContentPane().add(graficoHarmonico1);
		graficoHarmonico1.repaint();
		
		//GRÁFICO ONDA HARMONICO 2
		CalculoSimulacao02 ondaHarmonico2 = new CalculoSimulacao02();
		ArrayList<Double> grafico7 = ondaHarmonico2.ondaHarmonico2();
		Grafico graficoHarmonico2 = new Grafico(grafico7);
		graficoHarmonico2.setBounds(85,240,400,40);
		mainFrame1.getContentPane().add(graficoHarmonico2);
		graficoHarmonico2.repaint();
		
		//GRÁFICO ONDA HARMONICO 3
		CalculoSimulacao02 ondaHarmonico3 = new CalculoSimulacao02();
		ArrayList<Double> grafico8 = ondaHarmonico3.ondaHarmonico3();
		Grafico graficoHarmonico3 = new Grafico(grafico8);
		graficoHarmonico3.setBounds(85,290,400,40);
		mainFrame1.getContentPane().add(graficoHarmonico3);
		graficoHarmonico3.repaint();
		
		//GRÁFICO ONDA HARMONICO 4
		CalculoSimulacao02 ondaHarmonico4 = new CalculoSimulacao02();
		ArrayList<Double> grafico9 = ondaHarmonico4.ondaHarmonico2();
		Grafico graficoHarmonico4 = new Grafico(grafico9);
		graficoHarmonico4.setBounds(85,340,400,40);
		mainFrame1.getContentPane().add(graficoHarmonico4);
		graficoHarmonico4.repaint();
		
		//GRÁFICO ONDA HARMONICO 5
		CalculoSimulacao02 ondaHarmonico5 = new CalculoSimulacao02();
		ArrayList<Double> grafico10 = ondaHarmonico5.ondaHarmonico2();
		Grafico graficoHarmonico5 = new Grafico(grafico10);
		graficoHarmonico5.setBounds(85,390,400,40);
		mainFrame1.getContentPane().add(graficoHarmonico5);
		graficoHarmonico5.repaint();
		
		//GRÁFICO ONDA HARMONICO 6
		CalculoSimulacao02 ondaHarmonico6 = new CalculoSimulacao02();
		ArrayList<Double> grafico11 = ondaHarmonico6.ondaHarmonico6();
		Grafico graficoHarmonico6 = new Grafico(grafico11);
		graficoHarmonico6.setBounds(85,440,400,40);
		mainFrame1.getContentPane().add(graficoHarmonico6);
		graficoHarmonico6.repaint();
		
		
		
		
		//BOTÃO OK - USARA PARA O PRIMEIRO ACTION LISTENER
		JButton btnOk = new JButton("OK");
		btnOk.setFont(new Font("Lato", Font.BOLD, 14));
		btnOk.setBounds(500, 110, 60, 30);
		entradasComponenteFundamental1.add(btnOk);
		btnOk.repaint();
		btnOk.setActionCommand("OK");
		
		//BOTAO OK 2 --- IMPORTANTE ---
		btnOk2 = new JButton("OK!");
		btnOk2.setFont(new Font("Lato", Font.BOLD, 14));
		btnOk2.setBounds(506, 260, 60, 30);
		panelEntradasHarmonicas.add(btnOk2);
		btnOk2.repaint();
		btnOk2.setActionCommand("OK2");
		
		//COMBOBOX NUMERO HARMONICAS
		JComboBox comboBoxNumeroHarmonicas = new JComboBox();
		comboBoxNumeroHarmonicas.setBounds(154, 44, 50, 24);
		panelEntradasHarmonicas.add(comboBoxNumeroHarmonicas);
		comboBoxNumeroHarmonicas.repaint();
		
		for (int i = 1; i<7; i++){
            comboBoxNumeroHarmonicas.addItem(i);
            
		}
		
		//listener do ok 1
		btnOk.addActionListener(new Simulacao02Controler(
				
				comboBoxNumeroHarmonicas,
				mainFrame1, textAmplitudeFundamental,textAnguloDeFase,graficoHarmonico1, graficoHarmonico2, graficoHarmonico3, graficoHarmonico4, graficoHarmonico5, graficoHarmonico6,graficoComponenteFundamental,textAmplitude1, graficoResultante,textOrdem1,textAngulo1,
				textAmplitude2,	textOrdem2,	textAngulo2,
				textAmplitude3, textOrdem3,	textAngulo3,
				textAmplitude4, textOrdem4, textAngulo4,
				textAmplitude5, textOrdem5, textAngulo5, 
				textAmplitude6, textOrdem6, textAngulo6
				
				));
		
		//listener do ok2
		btnOk2.addActionListener(new Simulacao02Controler(
				
				comboBoxNumeroHarmonicas,
				mainFrame1, textAmplitudeFundamental,textAnguloDeFase,graficoHarmonico1, graficoHarmonico2, graficoHarmonico3, graficoHarmonico4, graficoHarmonico5,graficoHarmonico6,graficoComponenteFundamental,textAmplitude1, graficoResultante,textOrdem1,textAngulo1,
				textAmplitude2,	textOrdem2,	textAngulo2,
				textAmplitude3, textOrdem3,	textAngulo3,
				textAmplitude4, textOrdem4, textAngulo4,
				textAmplitude5, textOrdem5, textAngulo5, 
				textAmplitude6, textOrdem6, textAngulo6
				));
		
		
		
		
			
	}
}
